// FORM VALIDATION

// Get variables
const firstName = document.getElementById('firstName');
const lastName = document.getElementById('lastName');
const email = document.getElementById('email');
const pwd = document.getElementById('pwd');
const submit = document.getElementById('submit');


// Input names
const name = 'First Name';
const last = 'Last Name';
const emailAdd= 'Email';
const password = 'Password';

// Error messages
const errorTxt1 = ' cannot be empty';
const errorTxt2 = 'Looks like this is not an email';

// Check if the form was submitted
submit.addEventListener('click', function() {
 
    //Get all fields and make an array from the nodelist
    const fields = document.querySelectorAll('.inputField');
    const fieldsArr = Array.from(fields);
    //console.log(fieldsArr);

   // Loop through fieldsArr and check each input. Is it empty?
    fieldsArr.forEach((current) => {
      // If empty
      if(current.value == '') {
        // display error message, add red border and error icon
        // on focus - remove msg, border and icon
        if(current.id == 'firstName'){
          displayErrorMsg(name, firstName, errorTxt1, 1);
          removeErrorMsg(firstName, 1);
        }
        if (current.id == 'lastName') {
          displayErrorMsg(last, lastName, errorTxt1, 2);
          removeErrorMsg(lastName, 2);
        }    
        if (current.id == 'email') {
          displayErrorMsg(emailAdd, email, errorTxt1, 3);
          removeErrorMsg(email, 3);
        }    
        if (current.id == 'pwd') {
          displayErrorMsg(password, pwd, errorTxt1, 4);
          removeErrorMsg( pwd, 4);
        }    
  
      } else {
          // In not empty -> Check email format
          if(current.id == 'email') {
            const valid = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/.test(email.value);
            
          // Not valid?
            if (!valid) {
              // display error msg, add red border and error icon
              displayErrorMsg( '', email , errorTxt2, 3);
              // on focus clear input, remove error msg, border, icon
              removeErrorMsg(email, 3);
            } else {
            // Is valid?
              return email.value;
            }
          }  
      } 
    });


    // SHOW ERROR MESSAGE
     function displayErrorMsg(inputName, inputField, txt, num) {
        let errorEl, errorMsg;
        
        // Create span element
        errorEl = document.createElement('span');
        errorMsg = document.createTextNode(inputName + txt);
        errorEl.appendChild(errorMsg);
        //console.log(errorEl);

        // Set class and id attributes for span
        errorEl.setAttribute('class', 'error' );
        errorEl.setAttribute('id', 'err-' + num);

        // Insert newly created element after input field, add invalid class
        inputField.insertAdjacentElement('afterend', errorEl);
        inputField.classList.add('invalid');
      }
     

     // REMOVE ERROR MESSAGE
    function removeErrorMsg(inputField, num) {
      let span = document.getElementById('err-' + num);
      //console.log(span);

      submit.addEventListener('click', function() {
        span.remove();
      });

      // On focus   
      inputField.addEventListener('focus', function() {
        // Remove border and icon ( -> remove invalid class)
        this.classList.remove('invalid');

        // Clear input
        this.value = '';

        // Remove error msg (-> span element)
        span.remove();
      });
    }
});

